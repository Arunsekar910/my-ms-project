package com.advento.printnumberms.service.impl;

import org.springframework.stereotype.Service;

import com.advento.printnumberms.service.PrintNumberService;

@Service(value = "printNumberServiceImpl")
public class PrintNumberServiceImpl implements PrintNumberService{
	public String printNum() {
		String a="";
		for(int i=1; i<=100; i++) {
			a=a+i+" ";
		}
		return a;
	}
	
	
	public String printNum1() {
		String b="";
		for(int i=1; i<=100; i++) {
			if(i%2==0) {
				b=b+i+" ";
			}
		}
		return b;
	}
	
	public String printNum2() {
		String c="";
		for(int i=1; i<=100; i++) {
			if(i%2!=0) {
				c=c+i+" ";
			}
		}
		return c;
	}
	
	public String printNum3() {
		String d="";
		for(int i=100; i>=1; i--) {
			d=d+i+" ";
		}
		return d;
	}
	
	public String printNum4() {
		String e="";
		for(int i=20; i<=55; i++) {
			if(i%2==0)
			e=e+i+" ";
		}
		return e;
	}
}
