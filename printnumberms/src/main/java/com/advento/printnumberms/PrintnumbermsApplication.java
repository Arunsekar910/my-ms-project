package com.advento.printnumberms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PrintnumbermsApplication {

	public static void main(String[] args) {
		SpringApplication.run(PrintnumbermsApplication.class, args);
	}

}
