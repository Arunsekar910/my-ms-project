package com.advento.printnumberms.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.advento.printnumberms.service.PrintNumberService;

@RestController
public class PrintNumberController {
	@Autowired
	@Qualifier(value = "printNumberServiceImpl")
	private PrintNumberService printNumberService;

	@GetMapping(value = "/number")
	public ResponseEntity<String> printNum() {
		String number = printNumberService.printNum();
		ResponseEntity<String> responseEntity = new ResponseEntity<String>(number, HttpStatus.OK);
		return responseEntity;
	}

	@GetMapping(value = "/number1")
	public ResponseEntity<String> printNum1() {
		String number1 = printNumberService.printNum1();
		ResponseEntity<String> responseEntity = new ResponseEntity<String>(number1, HttpStatus.OK);
		return responseEntity;
	}

	@GetMapping(value = "/number2")
	public ResponseEntity<String> printNum2() {
		String number2 = printNumberService.printNum2();
		ResponseEntity<String> responseEntity = new ResponseEntity<String>(number2, HttpStatus.OK);
		return responseEntity;
	}

	@GetMapping(value = "/number3")
	public ResponseEntity<String> printNum3() {
		String number3 = printNumberService.printNum3();
		ResponseEntity<String> responseEntity = new ResponseEntity<String>(number3, HttpStatus.OK);
		return responseEntity;
	}
	
	@GetMapping(value = "/number4")
	public ResponseEntity<String> printNum4() {
		String number4 = printNumberService.printNum4();
		ResponseEntity<String> responseEntity = new ResponseEntity<String>(number4, HttpStatus.OK);
		return responseEntity;
	}
}