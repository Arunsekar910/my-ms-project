package com.advento.employee.service.impl;

import org.springframework.stereotype.Service;
import com.advento.employee.dto.EmployeeDTO;
import com.advento.employee.service.EmployeeService;

@Service(value = "employeeServiceImpl")
public class EmployeeServiceImpl implements EmployeeService {

	@Override
	public EmployeeDTO[] getEmployees() {
		EmployeeDTO employeeDTO1 = createEmployeeObj(1, "arun", 25, "IT");
		EmployeeDTO employeeDTO2 = createEmployeeObj(2, "balu", 26, "CSE");        
		EmployeeDTO employeeDTO3 = createEmployeeObj(3, "kavin", 24, "ECE");
		EmployeeDTO employeeDTO4 = createEmployeeObj(4, "guna", 23, "EEE");

		EmployeeDTO[] employeeDTOs = new EmployeeDTO[4];
		employeeDTOs[0] = employeeDTO1;
		employeeDTOs[1] = employeeDTO2;
		employeeDTOs[2] = employeeDTO3;
		employeeDTOs[3] = employeeDTO4;
		return employeeDTOs;

	}

	private EmployeeDTO createEmployeeObj(int id, String name, int age, String dep) {
		EmployeeDTO employeeDTO = new EmployeeDTO();
		employeeDTO.setId(id);
		employeeDTO.setName(name);
		employeeDTO.setAge(age);
		employeeDTO.setDep(dep);
		return employeeDTO;
	}
}
