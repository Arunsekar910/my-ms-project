package com.advento.employee.service;

import com.advento.employee.dto.EmployeeDTO;

public interface EmployeeService {
	public EmployeeDTO[] getEmployees();

}
