package com.advento.numberpatternms.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import com.advento.numberpatternms.service.NumberPatternService;

@RestController
public class NumberPatternController {
	@Autowired
	@Qualifier(value = "NumberPatternServiceImpl")
	private NumberPatternService numberPatternService;

	@GetMapping(value = "/numPattern")
	public ResponseEntity<String> numberPattern() {
		String numPattern = numberPatternService.numberPattern();
		ResponseEntity<String> responseEntity = new ResponseEntity<String>(numPattern, HttpStatus.OK);
		return responseEntity;
	}

	@GetMapping(value = "/numPattern1")
	public ResponseEntity<String> numberPattern1() {
		String numPattern1 = numberPatternService.numberPattern1();
		ResponseEntity<String> responseEntity = new ResponseEntity<String>(numPattern1, HttpStatus.OK);
		return responseEntity;
	}

	@GetMapping(value = "/numPattern2")
	public ResponseEntity<String> numberPattern2() {
		String numPattern2 = numberPatternService.numberPattern2();
		ResponseEntity<String> responseEntity = new ResponseEntity<String>(numPattern2, HttpStatus.OK);
		return responseEntity;
	}
}
