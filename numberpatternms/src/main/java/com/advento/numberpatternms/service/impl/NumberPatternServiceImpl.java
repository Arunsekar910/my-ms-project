package com.advento.numberpatternms.service.impl;

import org.springframework.stereotype.Service;

import com.advento.numberpatternms.service.NumberPatternService;

@Service(value = "NumberPatternServiceImpl")
public class NumberPatternServiceImpl implements NumberPatternService {
	@Override
	public String numberPattern() {
		String a = "";
		for (int i = 0; i <= 5; i++) {
			for (int j = 0; j < i; j++) {
				a += (i + " ");
			}
			a += ("\n");
		}
		return a;
	}

	@Override
	public String numberPattern1() {
		String b = "";
		int n = 1;
		for (int i = 0; i <= 5; i++) {
			for (int j = 0; j < i; j++) {
				b += (n + " ");
				n++;
			}
			b += ("\n");
		}
		return b;
	}

	public String numberPattern2() {
		String c = "";
		for (int i = 0; i <= 5; i++) {
			for (int j = 1; j < i; j++) {
				c += (j + " ");
			}
			c += ("\n");
		}
		return c;
	}
}
