package com.advento.starpatternms.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.advento.starpatternms.service.StarPatternService;

@RestController
public class StarPatternController {

	@Autowired
	@Qualifier(value = "starPatternServiceImpl")
	private StarPatternService starPatternService;

//	private StarPatternService starPatternService = new StarPatternServiceImpl();
	@GetMapping(value = "/pattern")
	public ResponseEntity<String> getLoop(@RequestParam(value = "source", required = false) String source,
			@RequestParam(value = "length") int length) {
		String pattern = starPatternService.getLoop(source, length);
		ResponseEntity<String> responseEntity = new ResponseEntity<String>(pattern, HttpStatus.OK);
		return responseEntity;
	}

	@GetMapping(value = "/pattern1")
	public ResponseEntity<String> getLoop1(@RequestParam(value = "source", required = false) String source,
			@RequestParam(value = "length") int length) {
		String pattern1 = starPatternService.getLoop1(source, length);
		ResponseEntity<String> responseEntity = new ResponseEntity<String>(pattern1, HttpStatus.OK);
		return responseEntity;
	}

	@GetMapping(value = "/pattern2")
	public ResponseEntity<String> getLoop2() {
		String pattern2 = starPatternService.getLoop2();
		ResponseEntity<String> responseEntity = new ResponseEntity<String>(pattern2, HttpStatus.OK);
		return responseEntity;
	}

	@GetMapping(value = "/pattern3")
	public ResponseEntity<String> getLoop3() {
		String pattern3 = starPatternService.getLoop3();
		ResponseEntity<String> responseEntity = new ResponseEntity<String>(pattern3, HttpStatus.OK);
		return responseEntity;
	}

	@GetMapping(value = "/pattern4")
	public ResponseEntity<String> getLoop4() {
		String pattern4 = starPatternService.getLoop4();
		ResponseEntity<String> responseEntity = new ResponseEntity<String>(pattern4, HttpStatus.OK);
		return responseEntity;
	}
}