package com.advento.starpatternms.service;

public interface StarPatternService {
	public String getLoop(String source, int length);
	public String getLoop1(String source, int length);
	public String getLoop2();
	public String getLoop3();
	public String getLoop4();

}
