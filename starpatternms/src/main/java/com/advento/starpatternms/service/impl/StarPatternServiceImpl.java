package com.advento.starpatternms.service.impl;

import org.springframework.stereotype.Service;

import com.advento.starpatternms.service.StarPatternService;

@Service(value = "starPatternServiceImpl")
public class StarPatternServiceImpl implements StarPatternService {
	@Override
	public String getLoop(String source, int length) {
		String k = "";
		if (length > 0) {

			for (int i = 1; i <= length; i++) {
				for (int j = 1; j <= i; j++) {
					k = k + "*";
				}
				if (source != null && source.equalsIgnoreCase("postman")) {
					k = k + "\n";
				} else {
					k = k + "<br />";
				}
			}
		}
		return k;
	}

	@Override
	public String getLoop1(String source, int length) {
		String l = "";
		if (length > 0) {
			for (int i = 1; i <= length; i++) {
				for (int j = length; j >= i; j--) {
					l = l + "*";
				}
				if (source != null && source.equalsIgnoreCase("postman")) {
					l = l + "\n";
				} else {
					l = l + "<br />";
				}
			}
		}
		return l;
	}

	public String getLoop2() {
		int i, j, row = 6;
		String a = "";
		for (i = 0; i < row; i++) {
			for (j = row - i; j >= 0; j--) {
				a = a + " ";
			}
			for (j = 0; j <= i; j++) {
				a = a + "*";
			}
			a = a + "\n";
		}

		return a;
	}

	public String getLoop3() {
		String b = "";
		int i, j, row = 6;
		for (i = 1; i <= row; i++) {
			for (j = row - i; j >= 1; j--) {
				b = b + " ";
			}

			for (j = 1; j <= i; j++) {
				b = b + "* ";
			}
			b = b + "\n";
		}

		return b;
	}

	public String getLoop4() {
		String c = "";
		for (int i = 1; i <= 5; i++) {
			for (int j = 5; j > i; j--) {
				c += (" ");
			}
			for (int k = 1; k <= (i * 2) - 1; k++) {
				c += ("*");
			}
			c += ("\n");
		}
		for (int i = 5 - 1; i >= 1; i--) {
			for (int j = 5 - 1; j >= i; j--) {
				c += (" ");
			}
			for (int k = 1; k <= (i * 2) - 1; k++) {
				c += ("*");
			}

			c += ("\n");
		}
		return c;
	}
}
